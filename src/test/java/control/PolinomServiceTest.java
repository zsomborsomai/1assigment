package control;

import model.DivisionResult;
import model.Polinom;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

class PolinomServiceTest {

    @Test
    void testAddition() {
        assumeTrue(PolinomService.addition(new Polinom("x^2 + x"), new Polinom("x^3 + x^2 - x")).equals(new Polinom("x^3 + 2x^2")));
        assumeTrue(PolinomService.addition(new Polinom("x"), new Polinom("-x")).equals(new Polinom("")));
        assumeTrue(PolinomService.addition(new Polinom(""), new Polinom("-     x")).equals(new Polinom("-x")));
    }
    @Test
    void testSubtraction() {
        assumeTrue(PolinomService.subtraction(new Polinom("x^2 + x"), new Polinom("x^3 + x^2 - x")).equals(new Polinom("-x^3 + 2x")));
        assumeTrue(PolinomService.subtraction(new Polinom("x^2 + x"), new Polinom("x^2 + x")).equals(new Polinom("")));
        assumeTrue(PolinomService.subtraction(new Polinom("x"), new Polinom("")).equals(new Polinom("x")));
    }

    @Test
    void testMultiplication() {
        assumeTrue(PolinomService.multiplication(new Polinom("x^2 + x"), new Polinom("x - x^3")).equals(new Polinom("-x^5 -x^4 + x^3 + x^2")));
        assumeTrue(PolinomService.multiplication(new Polinom("x^2 + x"), new Polinom("0")).equals(new Polinom("")));
        assumeTrue(PolinomService.multiplication(new Polinom("x"), new Polinom("x + 1")).equals(new Polinom("x^2 + x")));
    }

    @Test
    void testDerivate() {
        assumeTrue(PolinomService.derivate(new Polinom("x^2 + x")).equals(new Polinom("2x + 1")));
        assumeTrue(PolinomService.derivate(new Polinom("")).equals(new Polinom("")));
        assumeTrue(PolinomService.derivate(new Polinom("x")).equals(new Polinom("1")));
    }

    @Test
    void testIntegrate() {
        assumeTrue(PolinomService.integrate(new Polinom("3x^2 + x")).equals(new Polinom("x^3 + 0.5*x^2")));
        assumeTrue(PolinomService.integrate(new Polinom("")).equals(new Polinom("")));
        assumeTrue(PolinomService.integrate(new Polinom("x")).equals(new Polinom("0.5*x^2")));
    }

    @Test
    void testDivision() {
        assumeTrue(PolinomService.division(new Polinom("x^3 - 2x^2 + 6x -5"), new Polinom("x^2 - 1")).equals(new DivisionResult(new Polinom("x-2"), new Polinom("7x -7"))));
        assumeTrue(PolinomService.division(new Polinom("x^3 - 2x^2 + 6x"), new Polinom("x")).equals(new DivisionResult(new Polinom("x^2 - 2x^1 + 6 "), new Polinom(""))));
        assumeTrue(PolinomService.division(new Polinom("x^2 - 1"), new Polinom("x - 1")).equals(new DivisionResult(new Polinom("x + 1"), new Polinom(""))));
        assumeTrue(PolinomService.division(new Polinom("4x^2 + 1"), new Polinom("2")).equals(new DivisionResult(new Polinom("2x^2 + 0.5"), new Polinom(""))));
    }
}