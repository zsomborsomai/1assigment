import control.Controler;
import gui.ViewPoli;
import model.Model;

public class Main {

        public static void main(String args[]){
                ViewPoli view = new ViewPoli();
                Model model = new Model();
                Controler controler = new Controler(model, view);
                view.initGUI();
    }
}
