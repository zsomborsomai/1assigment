package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ErrorFrame extends JFrame{
    private JButton OKButton;
    private JLabel errorMessage;
    private JPanel panel1;

    public ErrorFrame(String errorMessage) throws HeadlessException {
        super("Error");
        JFrame.setDefaultLookAndFeelDecorated(true);
        this.setSize(560 , 200);
        this.setContentPane(panel1);
        this.errorMessage.setText(errorMessage);
    }

    public void addCloseListener(ActionListener mal) {
        OKButton.addActionListener(mal);
    }

}
