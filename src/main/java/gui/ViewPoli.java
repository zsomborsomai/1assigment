package gui;

import gui.ErrorFrame;
import gui.MainFrame;

import javax.swing.*;

public class ViewPoli {
    private String p1Str;
    private String p2Str;
    private MainFrame currentFrame;
    private ErrorFrame errorFrame;

    public ViewPoli() {
            currentFrame = new MainFrame();
    }

    public String getP1Str() {
        return p1Str;
    }

    public void setP1Str(String p1Str) {
        this.p1Str = p1Str;
    }

    public String getP2Str() {
        return p2Str;
    }

    public void setP2Str(String p2Str) {
        this.p2Str = p2Str;
    }

    public void initGUI(){
        currentFrame.setVisible(true);
        currentFrame.setLocationRelativeTo(null);
    }


    public MainFrame getCurrentFrame() {
        return currentFrame;
    }


    public void createErrorWindow(String str){
        errorFrame = new ErrorFrame(str);
        errorFrame.setVisible(true);
        errorFrame.setLocationRelativeTo(null);
        errorFrame.addCloseListener(e -> {
            errorFrame.dispose();
        });
    }
}
