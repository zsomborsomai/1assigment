package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class MainFrame extends JFrame {
    private JTextField Polynom1TextField;
    private JTextField Polynom2TextField;
    private JButton divideButton;
    private JButton subtractButton;
    private JButton multiplicationButton;
    private JButton addButton;
    private JButton exitButton;
    private JLabel Polynom1Label;
    private JPanel panel1;
    private JButton derivateButton;
    private JButton integrateButton;
    private JLabel resultLabel;

    public MainFrame() throws HeadlessException {
        super("Polinom calculator");
        JFrame.setDefaultLookAndFeelDecorated(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1080 , 720);
        this.setContentPane(panel1);

        exitButton.addActionListener(e -> {
            System.out.println("almaaaaa");
            System.exit(0);
        });
    }

    public JTextField getPolynom1TextField() {
        return Polynom1TextField;
    }

    public JTextField getPolynom2TextField() {
        return Polynom2TextField;
    }

    public void addMultiplyListener(ActionListener mal) {
        multiplicationButton.addActionListener(mal);
    }

    public void addSubbtractionListener(ActionListener mal) {
        subtractButton.addActionListener(mal);
    }

    public void addAdditionListener(ActionListener mal) {
        addButton.addActionListener(mal);
    }

    public void addDivisionListener(ActionListener mal) {
        divideButton.addActionListener(mal);
    }

    public void addDerivationListener(ActionListener mal) {
        derivateButton.addActionListener(mal);
    }

    public void addIntegrationListener(ActionListener mal) {
        integrateButton.addActionListener(mal);
    }

    public void setPolynomResultContent(String result){
        resultLabel.setText(result);
    }

}
