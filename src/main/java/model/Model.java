package model;

public class Model {
    private Polinom p1;
    private Polinom p2;
    private Polinom pRes;
    private DivisionResult divisionResult;

    public Polinom getP1() {
        return p1;
    }

    public void setP1(String p1Str) {
        if(p1Str != null){
            this.p1 = new Polinom(p1Str);
        }else {
            this.p2 = new Polinom("0");
        }
    }

    public Polinom getP2() {
        return p2;
    }

    public void setP2(String p2Str) {
        if(p2Str != null){
            this.p2 = new Polinom(p2Str);
        } else {
            this.p2 = new Polinom("0");
        }
    }

    public Polinom getpRes() {
        return pRes;
    }

    public void setpRes(Polinom pRes) {
        this.pRes = pRes;
    }

    public DivisionResult getDivisionResult() {
        return divisionResult;
    }

    public void setDivisionResult(DivisionResult divisionResult) {
        this.divisionResult = divisionResult;
    }
}
