package model;

public class DivisionResult {
    private Polinom quotient;
    private Polinom remainder;

    public DivisionResult(Polinom quotient, Polinom remainder) {
        this.quotient = quotient;
        this.remainder = remainder;
    }

    public Polinom getQuotient() {
        return quotient;
    }

    public void setQuotient(Polinom quotient) {
        this.quotient = quotient;
    }

    public Polinom getRemainder() {
        return remainder;
    }

    public void setRemainder(Polinom remainder) {
        this.remainder = remainder;
    }

    @Override
    public boolean equals(Object o){
        if(o == this) return true;
        if(!(o instanceof DivisionResult)) return false;
        if(((DivisionResult) o).getQuotient().equals(this.quotient) && ((DivisionResult) o).getRemainder().equals(this.remainder)){
            return true;
        }
        return false;
    }
}
