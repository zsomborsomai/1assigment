package model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class Polinom {
    public LinkedList<Monom> content = new LinkedList<Monom>();
    private int rank;

    public Polinom() {
    }

    public Polinom(String str) {
        str = str.replace(" ", "");
        str = str.replace("*", "");
        str = str.replace("+", " +");
        str = str.replace("-", " -");
        System.out.println(str);

        int maxRank = -1;
        StringTokenizer st = new StringTokenizer(str," ");
        LinkedList<Monom> monoms = new LinkedList<>();
        while (st.hasMoreElements())
        {
            content.add(new Monom(st.nextToken()));
            if(content.getLast().getRank() > maxRank) {
                maxRank = content.getLast().getRank();
            }
        }
        rank = maxRank;
        this.simplify();
    }


    public void simplify() {
        float[] Arr = new float[this.rank + 1];
        for(Monom m : content){
            Arr[m.getRank()] += m.getMultiplier();
        }
        LinkedList<Monom> newContent = new LinkedList<Monom>();
        for(int i = 0; i <= rank; i++){
            if(Arr[i] != 0){
                newContent.add(new Monom(i, Arr[i]));
            }
        }
        Collections.reverse(newContent);
        content = newContent;
        this.setRank();
    }

    public void addNewMonom(int rank, float multiplier){
        content.add(new Monom(rank, multiplier));
    }
    public void addNewMonom(Monom m1){
        if(m1.getRank() > this.rank) this.rank = m1.getRank();
        content.add(m1);
    }

    public void print(){
        for(Monom monom: content){
            monom.print();
        }
    }

    public String returnAsString(){
        String str = "";
        for(Monom monom: content){
            str += monom.returnAsString() + " ";
        }
        if(str == "") str = "0";
        return str;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public void setRank() {
        int rank = 0;
        for(Monom monom: content) {
            if(rank < monom.getRank()) {
                rank = monom.getRank();
            }
        }
        this.rank = rank;
    }

    public Monom getMonomByRank(int rank) {
        ///todo binary search!!!
        for(Monom m : content){
            if(m.getRank() == rank) return m;
        }
        return null;
    }
    @Override
    public boolean equals(Object o) {
        if(o == this) return true;
        if(!(o instanceof Polinom)) return false;
        ((Polinom) o).simplify();
        this.simplify();
        if(this.rank != ((Polinom) o).rank) return false;
        for(Monom m : this.content) {
            boolean isInObject = false;
            for(Monom m2 : ((Polinom) o).content){
                if(m2.equals(m)){
                    isInObject = true;
                    break;
                }
            }
            if(!isInObject)
                return false;
        }
        return true;
    }
}
