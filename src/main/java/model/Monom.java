package model;

import org.jetbrains.annotations.NotNull;

import java.util.StringTokenizer;

public class Monom implements Comparable<Monom>{
    private int rank = 1;
    private float multiplier = 1;


    public Monom(int rank, float multiplier) {
        this.rank = rank;
        this.multiplier = multiplier;
    }

    public Monom(String str) {
        ///2x^2    -3x^4      +5x^18   x^2     -x^3    -3
        int ind = str.indexOf('x');
        if(ind == -1){
            multiplier = Float.parseFloat(str);
            rank = 0;
            return;
        }
        StringTokenizer st = new StringTokenizer(str,"x^");
        if(ind != 0){
            String multiplierStr = st.nextToken();
            if(multiplierStr.equals("+")) multiplierStr = "+1";
            else if(multiplierStr.equals("-")) multiplierStr = "-1";
            multiplier = Float.parseFloat(multiplierStr);

        }
        if(st.hasMoreTokens()) {
            rank = Integer.parseInt(st.nextToken());
        }
    }

    public int getRank() {
        return rank;
    }

    public float getMultiplier() {
        return multiplier;
    }

    public void print(){
        System.out.println("model.Monom: " + this.multiplier + "x^" + this.rank);
    }

    public void setMultiplier(float multiplier) {
        this.multiplier = multiplier;
    }

    public String returnAsString(){
        String str = "";
        if(multiplier > 0){
            str = "+";
        }
        str += this.multiplier;
        if(this.rank != 0){
            str +="x^" + this.rank;
        }
        return str;
    }
    @Override
    public int compareTo(@NotNull Monom o) {
        if(this.rank > o.getRank()){
            return 1;
        }
        else if(this.rank == o.getRank() &&this.multiplier >= o.getMultiplier()){
            return 1;
        }
        return -1;
    }
    @Override
    public boolean equals(Object o){
        if(o == this) return true;
        if(!(o instanceof Monom)) return false;
        if(((Monom) o).getRank() == rank && ((Monom) o).getMultiplier() == multiplier) return true;
        return false;
    }
}
