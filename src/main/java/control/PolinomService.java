package control;

import model.DivisionResult;
import model.Monom;
import model.Polinom;

public class PolinomService {

    public static Polinom addition(Polinom p1, Polinom p2){
        Polinom result = new Polinom();
        int maxRank = Math.max(p1.getRank(), p2.getRank());
        result.setRank(maxRank);
        for(int i = maxRank; i >= 0; i--){
            Monom m1 = p1.getMonomByRank(i);
            Monom m2 = p2.getMonomByRank(i);
            float multiplier = 0;
            if(m1 != null) multiplier+= m1.getMultiplier();
            if(m2 != null) multiplier+= m2.getMultiplier();
            if(multiplier != 0){
                result.content.add(new Monom(i, multiplier));
            }
        }
        result.simplify();
        return result;
    }

    public static Polinom subtraction(Polinom p1, Polinom p2){
        Polinom result = new Polinom();
        int maxRank = Math.max(p1.getRank(), p2.getRank());
        result.setRank(maxRank);
        for(int i = maxRank; i >= 0; i--){
            Monom m1 = p1.getMonomByRank(i);
            Monom m2 = p2.getMonomByRank(i);
            int multiplier = 0;
            if(m1 != null) multiplier+= m1.getMultiplier();
            if(m2 != null) multiplier-= m2.getMultiplier();
            if(multiplier != 0){
                result.content.add(new Monom(i, multiplier));
            }
        }
        result.simplify();
        return result;
    }

    public static Polinom derivate(Polinom p1){
        Polinom result = new Polinom();
        result.setRank(p1.getRank() - 1);
        for(Monom m : p1.content){
            if(m.getRank() - 1 > -1){
                result.addNewMonom(m.getRank() - 1, m.getMultiplier() * m.getRank());
            }
        }
        result.simplify();
        return result;
    }

    public static Polinom integrate(Polinom p1){
        Polinom result = new Polinom();
        result.setRank(p1.getRank() + 1);
        for(Monom m : p1.content){
            result.addNewMonom(m.getRank() + 1, m.getMultiplier() / (m.getRank() + 1));
        }
        return result;
    }

    public static Polinom multiplication(Polinom p1, Polinom p2) {
        p1.print();
        p2.print();
        Polinom result = new Polinom();
        result.setRank(p1.getRank() + p2.getRank());
        for (Monom m1 : p1.content) {
            for (Monom m2 : p2.content) {
                result.addNewMonom(m1.getRank() + m2.getRank(), m1.getMultiplier() * m2.getMultiplier());
            }
        }
        result.simplify();
        return result;
    }

    public static Monom divideMonom(Monom m1, Monom m2){
        return new Monom(m1.getRank() - m2.getRank(), m1.getMultiplier()/ m2.getMultiplier());
    }

    public static DivisionResult division(Polinom p1, Polinom p2) {
        if(p2.getRank() == 0){
            Polinom temp = p2;
            temp.content.getFirst().setMultiplier(1/temp.content.getFirst().getMultiplier());
            DivisionResult divisionResult = new DivisionResult(PolinomService.multiplication(p1,temp), new Polinom(""));
            return divisionResult;
        }
        Polinom quotient = new Polinom();
        Polinom remainder = p1;
        while(remainder.getRank() >= p2.getRank()){
            quotient.addNewMonom(divideMonom(remainder.content.getFirst(), p2.content.getFirst()));
            remainder.simplify();
            p2.simplify();

            Polinom temp = PolinomService.multiplication(quotient, p2);
            remainder = PolinomService.subtraction(p1,temp);
        }
        DivisionResult divisionResult = new DivisionResult(quotient, remainder);
        return divisionResult;
    }
}
