package control;

import control.PolinomService;
import gui.ViewPoli;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.*;


public class Controler {
        private Model model;
        private ViewPoli view;

        public Controler(Model model, ViewPoli view) {
        this.model = model;
        this.view = view;
        view.getCurrentFrame().addMultiplyListener(new MultiplierListener());
        view.getCurrentFrame().addIntegrationListener(new IntegrateListener());
        view.getCurrentFrame().addAdditionListener(new AdditionListener());
        view.getCurrentFrame().addDerivationListener(new DerivateListener());
        view.getCurrentFrame().addSubbtractionListener(new SubbtractionListener());
        view.getCurrentFrame().addDivisionListener(new DivisionListener());
    }

        class MultiplierListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                model.setP1(view.getCurrentFrame().getPolynom1TextField().getText());
                model.setP2(view.getCurrentFrame().getPolynom2TextField().getText());
                model.getP1().print();
                model.setpRes(PolinomService.multiplication(model.getP1(), model.getP2()));
                view.getCurrentFrame().setPolynomResultContent(model.getpRes().returnAsString());
            } catch (NumberFormatException nfex) {
                view.createErrorWindow("Wrong input!");
            }
        }
    }

        class AdditionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                model.setP1(view.getCurrentFrame().getPolynom1TextField().getText());
                model.setP2(view.getCurrentFrame().getPolynom2TextField().getText());
                model.getP1().print();
                model.setpRes(PolinomService.addition(model.getP1(), model.getP2()));
                view.getCurrentFrame().setPolynomResultContent(model.getpRes().returnAsString());
            } catch (NumberFormatException nfex) {
                view.createErrorWindow("Wrong input!");
            }
        }
    }

        class SubbtractionListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    model.setP1(view.getCurrentFrame().getPolynom1TextField().getText());
                    model.setP2(view.getCurrentFrame().getPolynom2TextField().getText());
                    model.getP1().print();
                    model.setpRes(PolinomService.subtraction(model.getP1(), model.getP2()));
                    view.getCurrentFrame().setPolynomResultContent(model.getpRes().returnAsString());
                } catch (NumberFormatException nfex) {
                    view.createErrorWindow("Wrong input!");
                }
            }
        }

        class DivisionListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    model.setP1(view.getCurrentFrame().getPolynom1TextField().getText());
                    model.setP2(view.getCurrentFrame().getPolynom2TextField().getText());
                    model.getP1().print();
                    model.setDivisionResult(PolinomService.division(model.getP1(), model.getP2()));
                    view.getCurrentFrame().setPolynomResultContent(model.getDivisionResult().getQuotient().returnAsString() + "  Remainder: " + model.getDivisionResult().getRemainder().returnAsString());
                } catch (NumberFormatException nfex) {
                    view.createErrorWindow("Wrong input!");
                }
            }
        }

        class DerivateListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    model.setP1(view.getCurrentFrame().getPolynom1TextField().getText());
                    model.setP2(view.getCurrentFrame().getPolynom2TextField().getText());
                    model.getP1().print();
                    model.setpRes(PolinomService.derivate(model.getP1()));
                    view.getCurrentFrame().setPolynomResultContent(model.getpRes().returnAsString());
                } catch (NumberFormatException nfex) {
                    view.createErrorWindow("Wrong input!");
                }
            }
        }

        class IntegrateListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try
            {
                model.setP1(view.getCurrentFrame().getPolynom1TextField().getText());
                model.setP2(view.getCurrentFrame().getPolynom2TextField().getText());
                model.getP1().print();
                model.setpRes(PolinomService.integrate(model.getP1()));
                view.getCurrentFrame().setPolynomResultContent(model.getpRes().returnAsString());
            } catch (NumberFormatException nfex) {
                view.createErrorWindow("Wrong input!");
            }
        }
    }

}
